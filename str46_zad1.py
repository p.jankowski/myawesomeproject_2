class Parent:

    def __init__(self):
        self.name = "Andrej"
        self.surname = "Janos"
        self.age = "62"


class Child(Parent):

    def __init__(self):
        super().__init__()


child = Child()
print("My father's name is " + child.name)
print("My father's surname is " + child.surname)
print("My father's age is " + child.age)
