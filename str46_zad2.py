class Parent:
    def parent(self):
        print("Parent")

    def parent1(self):
        print("Parent1")

    def __init__(self):
        print("Parent init")


class Child(Parent):
    def children(self):
        print("Child ")


child = Child()
child.children()
child.parent()
child.parent1()
child.__init__()
